# Snippy

Git-hosted GitLab Snippets Management Tool. Snippy helps manage snippets from
one projects, to a directory structure that are tracked by Git.

Resources:
- Documentation

## Development Roadmaps
- MVP
  - [ ] Able to authenticate to GitLab.
  - [ ] Able to "pull" and "push" from Snippets.
  - [ ] Able to diff (with git)
  - [ ] Store config user-wide and project-wide.
  - [ ] Able to update Snippets description.
- Documentation
  - [ ] Man pages
  - [ ] Bash Auto-completion
- Maintainability
  - [ ] Unit Testing
  - [ ] Integration Test
  - [ ] Setup CI/CD for all of those tests
  - [ ] Dedicated project page (on Chris' homepage)
- Security
  - [ ] PGP Signature (preferable one for release, i.e
    open-source@christianto.net)
  - [ ] CodeSigning Certs (only when we are going to publish them on Windows)
- Distribution
  - [ ] Publish to APT
    - Required dependecies
      - git \
        TODO: Find the minimum version requirements.
  - [ ] Setup CI/CD for distribution

## Getting Started

*TBD*.

## Development

Check [CONTRIBUTING.md](./CONTRIBUTING.md) to start the development environment for this.

## License

[MIT](./LICENSE).
