pub trait Singleton {
    unsafe fn instance() -> Self;
}
