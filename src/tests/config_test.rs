use std::collections::HashMap;

use crate::{
    model::config::{SnippyConfig, Workspace},
    utils::config::{FromFile, Locatable},
};

#[test]
pub fn config_merge_test() {
    let config_a = SnippyConfig::default();
    let config_b = SnippyConfig {
        auth_path: Some("./test.json".to_string()),
        workspace: Some(Workspace(vec![
            "snippets-1".to_string(),
            "snippets-2".to_string(),
        ])),
        ..SnippyConfig::default()
    };

    let merged_config = config_a + config_b;

    assert!(merged_config.auth_path.is_some());
    assert_eq!(merged_config.auth_path.unwrap(), "./test.json".to_string());
    assert!(merged_config.workspace.is_some());
    assert_eq!(
        merged_config.workspace.unwrap(),
        Workspace(vec!["snippets-1".to_string(), "snippets-2".to_string(),])
    );
}

pub(self) struct MockConfig {
    path_result: HashMap<String, bool>,
}

impl MockConfig {
    pub(self) fn new(path_result: HashMap<String, bool>) -> Self {
        Self { path_result }
    }
}

impl FromFile for MockConfig {
    fn validate_file(&self, path: &String) -> bool {
        self.path_result.contains_key(path) && self.path_result[path]
    }

    fn load_file(&self, _path: &String) -> crate::model::error::Result<Self> {
        unreachable!()
    }
}

#[test]
pub fn config_search_test() {
    let path_set = HashMap::from([
        (
            "/home/chez14/workspaces/blog-snippets/.snippyrc".to_string(),
            true,
        ),
        (
            "/home/chez14/workspaces/blog-snippets-2/.snippyrc.json".to_string(),
            true,
        ),
    ]);

    let mocked_config = MockConfig::new(path_set);

    let tests = [
        (
            "/home/chez14/workspaces/blog-snippets/",
            Some("/home/chez14/workspaces/blog-snippets/.snippyrc"),
        ),
        (
            "/home/chez14/workspaces/blog-snippets/src/snippets-a",
            Some("/home/chez14/workspaces/blog-snippets/.snippyrc"),
        ),
        (
            "/home/chez14/workspaces/blog-snippets/src/snippets-b/",
            Some("/home/chez14/workspaces/blog-snippets/.snippyrc"),
        ),
        (
            "/home/chez14/workspaces/blog-snippets-2/src/snippets-a/",
            Some("/home/chez14/workspaces/blog-snippets-2/.snippyrc.json"),
        ),
        (
            "/home/chez14/workspaces/blog-snippets-2/",
            Some("/home/chez14/workspaces/blog-snippets-2/.snippyrc.json"),
        ),
        ("/home/chez14/workspaces/blog-snippets-3/", None),
        ("/home/chez14/workspaces/", None),
    ];

    tests.into_iter().for_each(|(test, expected)| {
        let mock_result = mocked_config.project_config_path(test.to_string());
        assert_eq!(mock_result.is_some(), expected.is_some());
        if expected.is_some() {
            assert_eq!(mock_result.unwrap(), expected.unwrap().to_string());
        }
    });
}
