use std::{
    fs::{self, File},
    path::{self, Path},
};

use crate::model::{
    config::{SnippyConfig, CONFIG_NAMESPACE_APP, CONFIG_NAMESPACE_ORG},
    error::{Error, Result},
};

pub trait FromFile {
    fn validate_file(&self, path: &String) -> bool;
    fn load_file(&self, path: &String) -> Result<Self> where Self: Sized;
}

pub trait Locatable {
    fn user_config_path(&self) -> Option<String>;
    fn project_config_path(&self, working_directory: String) -> Option<String>;
}

impl<T> Locatable for T
where
    T: FromFile,
{
    fn user_config_path(&self) -> Option<String> {
        #[cfg(target_os = "windows")]
        let homedir = "%USERPROFILE%";
        #[cfg(not(target_os = "windows"))]
        let homedir = "~";

        // read JSON file.
        let mut config_dir = Path::new(homedir).to_path_buf();

        config_dir = config_dir
            .join(".config")
            .join(CONFIG_NAMESPACE_ORG)
            .join(CONFIG_NAMESPACE_APP)
            .join("snippyrc.json");

        Some(config_dir.to_str().unwrap().to_string())
    }

    fn project_config_path(&self, working_directory: String) -> Option<String> {
        // string to find: `.snippyrc`, `.snippyrc.json`
        let file_to_find = [".snippyrc", ".snippyrc.json"];

        let mut current_path = String::from(working_directory);
        if !current_path.ends_with(path::MAIN_SEPARATOR) {
            current_path.push(path::MAIN_SEPARATOR);
        }
        let separator = path::MAIN_SEPARATOR.to_string();
        let path_split: Vec<String> = current_path.split(&separator).map(String::from).collect();

        let mut current_section = path_split.len();
        while current_section > 0 {
            let path = path_split[..current_section].join(&separator);
            for config_file in file_to_find {
                let config = path.clone() + &separator + config_file;

                if self.validate_file(&config) {
                    return Some(config);
                }
            }
            current_section -= 1;
        }
        return None;
    }
}

impl FromFile for SnippyConfig {
    fn validate_file(&self, path: &String) -> bool {
        fs::metadata(path).is_ok()
    }

    fn load_file(&self, path: &String) -> Result<Self> {
        let reader = File::open(&path);

        if reader.is_err() {
            return Err(Error::file_cannot_open(path.clone(), reader.err().unwrap()));
        }

        let parsed = serde_json::from_reader::<File, Self>(reader.unwrap());

        if parsed.is_err() {
            return Err(Error::malformed_json(path.clone(), &parsed.unwrap_err()));
        }

        return Ok(parsed.unwrap());
    }
}
