use crate::{
    model::{auth::SnippyAuthConfig, config::SnippyConfig},
    traits::singleton::Singleton,
};

use super::config::Locatable;

impl Locatable for SnippyAuthConfig {
    fn user_config_path(&self) -> Option<String> {
        let config = unsafe { SnippyConfig::instance() };
        config.auth_path
    }

    fn project_config_path(&self, _: String) -> Option<String> {
        None
    }
}
