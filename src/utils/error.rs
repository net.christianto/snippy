use std::io;

use crate::model::error::{Error, ErrorCode};

impl Error {
    pub fn malformed_json(file: String, error: &dyn std::error::Error) -> Error {
        Error {
            err_type: ErrorCode::JsonMalform,
            msg: format!(
                "User-specific configuration is malformed. Please check {}.",
                file
            ),
            context: error.to_string(),
        }
    }

    pub fn file_cannot_open(file: String, error: io::Error) -> Error {
        Error {
            err_type: ErrorCode::IOError(error),
            msg: format!("Error while reading {}", file),
            context: "".to_string(),
        }
    }
}
