use serde::{Deserialize, Serialize};
use std::{collections::HashMap, ops::Add};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SnippyAuthConfig {
    instances: Option<HashMap<String, AuthItem>>,
}

impl SnippyAuthConfig {
    pub fn get_auth_config(self, instance_name: String) -> Option<AuthItem> {
        let instances_list = &self.instances.unwrap();
        let config = instances_list.get::<String>(&instance_name);
        if config.is_none() {
            return None;
        }
        return Some(config.unwrap().clone());
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AuthItem {
    /// Instance name can be abritrary, meaning we can set the auth config to
    /// other name, but uses the same domain. We can do this with git (well, via
    /// SSH config too), so why don't we add the ability to that too?
    domain: Option<String>,

    /// Authentication token for this instance
    token: String,
}

impl Add<SnippyAuthConfig> for SnippyAuthConfig {
    type Output = SnippyAuthConfig;

    fn add(self, other: SnippyAuthConfig) -> Self::Output {
        SnippyAuthConfig {
            instances: self.instances.or(other.instances),
        }
    }
}
