use std::{fmt::Display, io};

#[derive(Debug)]
pub struct Error {
    pub msg: String,
    pub err_type: ErrorCode,
    pub context: String,
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum ErrorCode {
    JsonMalform,
    IOError(io::Error),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.err_type {
            ErrorCode::JsonMalform => f.write_fmt(format_args!(
                "JSON is malformed: {}\n{}",
                self.msg.as_str(),
                self.context
            )),
            ErrorCode::IOError(io) => f.write_fmt(format_args!(
                "IO Error: {}\n{}",
                self.msg.to_string(),
                io.to_string()
            )),
        }
    }
}
