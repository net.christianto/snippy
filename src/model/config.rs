use crate::{
    traits::singleton::Singleton,
    utils::config::{FromFile, Locatable},
};
use serde::{Deserialize, Serialize};
use std::{env::current_dir, ops::Add};

/*
We follow XDG Base Directory Specification to build the directory, for more
information on this, check {@see snippy::utils::get_user_config}.
*/
pub const CONFIG_NAMESPACE_ORG: &str = "chez14";
pub const CONFIG_NAMESPACE_APP: &str = "snippy";

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SnippyConfig {
    pub auth_path: Option<String>,
    pub workspace: Option<Workspace>,
    pub git: Option<Git>,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Workspace(pub Vec<String>);

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Git {
    pub diff_from: Option<String>,
}

pub static mut CONFIG: Option<SnippyConfig> = None;

impl Default for SnippyConfig {
    fn default() -> Self {
        Self {
            auth_path: Some("auth.json".to_string()),
            workspace: None,
            git: Some(Git {
                diff_from: Some("master".to_string()),
            }),
        }
    }
}

impl Add<SnippyConfig> for SnippyConfig {
    type Output = SnippyConfig;

    fn add(self, other: SnippyConfig) -> Self::Output {
        Self {
            auth_path: other.auth_path.or(self.auth_path),
            workspace: other.workspace.or(self.workspace),
            git: other.git.or(self.git),
        }
    }
}

impl Singleton for SnippyConfig {
    unsafe fn instance() -> Self {
        if CONFIG.is_none() {
            let config = Self::default().load_config();
            CONFIG = Some(config);
        }
        CONFIG.clone().unwrap()
    }
}

impl SnippyConfig {
    fn do_load_config(&self) -> Self {
        let mut config = Self::default();

        let user_config_path = self.user_config_path();
        if user_config_path.is_none() {
            return config;
        }
        let parsed = self.load_file(&user_config_path.unwrap());
        if parsed.is_err() {
            return config;
        }
        config = config + parsed.unwrap();
        let project_config_path =
            self.project_config_path(String::from(current_dir().unwrap().to_str().unwrap()));
        if project_config_path.is_none() {
            return config;
        }
        let parsed_project = self.load_file(&project_config_path.unwrap());

        if parsed_project.is_err() {
            return config;
        }

        config + parsed_project.unwrap()
    }

    pub fn load_config(&self) -> Self {
        let config = self.do_load_config();
        config
    }
}
